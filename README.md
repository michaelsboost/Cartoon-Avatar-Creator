Cartoon Avatar Creator
===================

Just a simple vector based cartoon avatar creator

![](https://raw.githubusercontent.com/michaelsboost/Cartoon-Avatar-Creator/gh-pages/screenshot.png)

Version
-------------

0.0.1

License
-------------

MIT

Availability
-------------

Currently, this app is only available as a web application.

In the meatime, if you want this as a desktop application you can use
[WebDGap](http://michaelsboost.github.io/WebDGap)  

Tech
-------------

Cartoon Avatar Creator uses a number of open source projects to work properly:

* [JQWidgets](http://www.jqwidgets.com/jquery-widgets-demo/demos/jqxsplitter/index.htm#demos/jqxsplitter/nested-splitters.htm) - Resizable columns
* [JQuery Mini Colors](https://labs.abeautifulsite.net/jquery-minicolors/) - Color picker
* [jQuery](http://jquery.com/) - Because the first two libraries abve require it
* [Normalize](https://github.com/necolas/normalize.css) - A css reset library
* [saveSvgAsPng](https://github.com/exupero/saveSvgAsPng) - Saves our SVG as a PNG
* [FileSaver.js](https://github.com/eligrey/FileSaver.js/) - Easy way to save files
* [SweetAlert2](https://sweetalert2.github.io/) - Stylish alert dialog
* [TinyColor](https://github.com/bgrins/TinyColor) - Converts color codes
* [Moveit](https://github.com/Raminsiach/Moveit) - SVG Path Animator

Development
-------------

Want to contribute? Great!  

The Cartoon Avatar Creator is no longer an active project however you can submit a pull request or simply share the project :)

Of course the app is free and open source, so you can always fork the project and have fun :)

If the Cartoon Avatar Creator was at all helpful for you. You can show your appreciation by [Donating via SquareCash](https://cash.me/$michaelsboost) and/or [PayPal](https://www.paypal.me/mikethedj4)
